/* Your answer here */

// Instructions
//
// use DOM traversal functions to navigate your way to the following elements and modify them as specified. Do not modify the dom.html file.

// 1. Make the first div pink.
// 2. Make the last paragraph element have red text.
// 3. Hide the 2nd div.
// 4. Change the text in the first paragraph to say "Hello World".
// 5. When you click on the button, make all paragraphs bold.
// Hint : you haven't learnt about JavaScript loops yet, but loops are not the only way to accomplish this. Have a look at http://www.w3schools.com/jsref/dom_obj_style.asp instead for ideas on how else you could accomplish this .
// If you get stuck at any point during the above exercise, remember to check the Web Inspector's Console tab for errors.

// Q1
document.getElementsByClassName("text1")[0].style.color = "pink";

// Q2
document.getElementById("footer").style.color = "red";

// Q3
document.getElementsByClassName("text2")[0].style.display = "none";

// Q4
document.getElementsByClassName("subtitle")[0].textContent = "Hello World";

// Q5
document.getElementById("makeBold").onclick = makeBold;

function makeBold () {
    document.querySelector('body').style.fontWeight = 'bold';
}